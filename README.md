# ansible-chgpasswd



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/alberto_fernandez/ansible-chgpasswd.git
git branch -M main
git push -uf origin main
```

## Collaborate with your team

- [x] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)


## Name
### Changing a User's Password with Ansible.

## Description
To change a user's password with Ansible, you need to provide the password in an encrypted format, which you can generate using openssl or python-passlib. Below, I'll show you how to generate an encrypted password and then include it in the playbook.

## Installation
For creating this Ansible playbook you need to perform 2 steps Generate Encrypted Password and create the Ansible Playbook.

### Step 1: Generate Encrypted Password
First, generate an encrypted password. You can do this on your command line. If you're using openssl, the command looks like this:
```bash
openssl passwd -salt 'a random salt' -1 'yourpassword'
```

Or, if you prefer using Python with passlib, you can generate a SHA-512 hashed password like this:
```python
from passlib.hash import sha512_crypt
print(sha512_crypt.hash("yourpassword", rounds=5000))
```
Replace 'yourpassword' with the actual password you want to set for the user.

### Step 2: Ansible Playbook
After you've generated the encrypted password, replace your_encrypted_password_here in the playbook below with your encrypted password.

```yml
---
- name: Change user password
  hosts: your_target_hosts
  become: yes
  vars:
    user_name: your_user                         # Replace with your user
    user_password: your_encrypted_password_here  # Replace with the encrypted password

  tasks:
    - name: Change password for the user
      ansible.builtin.user:
        name: "{{ user_name }}"
        password: "{{ user_password }}"
        update_password: always
```

This playbook performs the following actions:
- It targets your_target_hosts, which you should replace with your actual target hosts or host groups as defined in your inventory file.
- It uses privilege escalation (become: yes) because changing a user's password typically requires root privileges.
- It defines two variables, user_name and user_password, where user_password should be the encrypted password you generated in Step 1.
- It runs a task using the ansible.builtin.user module to change the password of the specified user.

Make sure to replace your_target_hosts with your actual target hosts and your_encrypted_password_here with the encrypted password you generated.


## Usage
To run the playbook, save it to a file (e.g., change-password.yml) and execute it with the ansible-playbook command:

```bash
ansible-playbook -u afernandez change-password.yml -kK
```
Ensure you have Ansible installed and configured properly to communicate with your target hosts..

## Support
If you got some issues with this playbook you can contact me on Slack or email at Alberto.Fernandez@ellucian.com

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
We greatly appreciate contributions of any kind - from bug fixes to enhancements. To ensure a smooth collaboration process, we've outlined some guidelines for contributing to our Ansible playbook repository.

### Reporting Issues

If you encounter any bugs or have suggestions for improvements, please file an issue through the GitLab issue tracker. Include as much detail as possible, such as:

- The expected behavior and what actually happened
- Steps to reproduce the issue
- Any relevant configurations or environment information

### Submitting Changes

To submit a change:

1. Fork the repository.
2. Create a new branch for your changes (`git checkout -b feature/your_feature_name`).
3. Make your changes. Ensure you adhere to the existing coding conventions and cover any new functionality with appropriate tests.
4. Commit your changes using clear and descriptive commit messages.
5. Push your changes to your fork (`git push origin feature/your_feature_name`).
6. Open a merge request against our repository. Include a clear description of the problem you're solving, any relevant issue numbers, and any testing you've performed.

### Code Review Process

Once you submit a merge request, the project maintainers will review your changes. We aim to review all contributions promptly. During the review, we might suggest changes, improvements, or alternatives. Your contributions will be merged into the project once the review process is complete.

### Setting Up Your Environment

For changes that require testing:

- Ensure you have Ansible installed on your machine.
- Run any existing tests to confirm they pass with your changes.
- Document any new dependencies or environment changes required by your contribution.

### Documentation

If your changes introduce new features or change existing ones, please update the documentation accordingly. This includes updating inline comments, README files, and any other relevant documentation.

### Community Guidelines

This project adheres to a code of conduct. By participating, you are expected to uphold this code. Please report unacceptable behavior to the project maintainers.

We look forward to your contributions!

## Authors and acknowledgment
Alberto Fernandez - Linux Engineer.

## Project status
As of [last update date], this Ansible playbook is in the `[Development/Testing/Stable/Deprecated]` phase. Below is a brief overview of the project status:

- **Development**: The playbook is actively being developed. New features and improvements are regularly added, and we welcome contributions and feedback.
- **Testing**: Currently undergoing extensive testing to ensure reliability and stability. Bug reports and fixes are highly appreciated during this phase.
- **Stable**: The playbook has matured and is considered stable for production use. Updates at this stage primarily focus on bug fixes and optimizations rather than new features.
- **Deprecated**: The playbook is no longer actively maintained. It may not work with newer versions of Ansible or the managed systems. We recommend looking for alternatives or forks of this project.

### Current Focus

- If in **Development** or **Testing**: We are currently focusing on [feature development, testing specific roles, improving security practices, etc.]. Your input and contributions towards these areas are highly valued.
- If in **Stable**: Our main focus is on maintaining the playbook's reliability, fixing any emerging bugs, and ensuring compatibility with the latest Ansible versions. Suggestions for minor improvements are welcome.
- If in **Deprecated**: This project is no longer actively maintained. However, we're open to community forks and will list them here for users looking for updated versions or alternatives.

### Contributing to Future Development

Regardless of the current phase, we encourage the community to contribute. Whether it's feature suggestions, bug reports, or direct code contributions, your feedback is invaluable for the ongoing development and maintenance of this playbook. See the [Contributing](#contributing) section for more details on how you can help.

### Stay Updated

For the latest updates, changes, and release notes, please check the project's [Changelog] or [Releases] section. We aim to keep our users and contributors well-informed about the project's progress and updates.

[Changelog]: # (Link to your changelog)
[Releases]: # (Link to your GitLab releases page)

---

We appreciate your interest in our project, and we're excited to see how it grows and evolves with your support!